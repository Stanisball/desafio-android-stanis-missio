package com.example.desafioandroid

import com.example.desafioandroid.viewmodel.CharacterDetailViewModel
import com.example.desafioandroid.viewmodel.ExpenseCharacterHQViewModel
import com.example.desafioandroid.viewmodel.MainActivityViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val viewModelModule = module {
    viewModel { MainActivityViewModel(get()) }
    viewModel { CharacterDetailViewModel(get()) }
    viewModel { ExpenseCharacterHQViewModel(get()) }
}

val repositoryModule = module {
    single { CharacterRepository(get()) }
}

val networkModule = module {
    single { provideOkHttpClient() }
    single { provideRetrofit(get()) }
    single { createRetrofit(get()) }
}

private fun provideOkHttpClient(): OkHttpClient {
    val logging = HttpLoggingInterceptor()
    logging.setLevel(HttpLoggingInterceptor.Level.BODY)

    val httpClient = OkHttpClient.Builder()
    httpClient.addInterceptor(logging)

    return httpClient.build()
}

private fun provideRetrofit(okHttp: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttp)
        .build()
}

private fun createRetrofit(retrofit: Retrofit) = retrofit.create(API::class.java)
