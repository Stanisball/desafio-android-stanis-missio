package com.example.desafioandroid

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.example.desafioandroid.model.Thumbnail

@BindingAdapter("app:glide")
fun glide(view: ImageView, thumbnail: Thumbnail?) {
    if (thumbnail != null) {
        Glide.with(view).load("${thumbnail.path}.${thumbnail.extension}").into(view)
    }
}