package com.example.desafioandroid.utils

import okhttp3.internal.and
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class APIUtils {

    companion object {
        val timeStamp = System.currentTimeMillis().toString()
        const val publicKey = "5e701320e67fa78f24da59a5078e7a0d"
        private const val privateKey = "6fa14baeef26497c7c2fde19fcc146e7c0796e62"
        private var md5Key = ""

        fun getMd5Key(): String {
            val input = "$timeStamp$privateKey$publicKey"
            try {
                val md: MessageDigest = MessageDigest.getInstance("MD5")
                val md5Bytes: ByteArray = md.digest(input.toByteArray())
                val md5 = StringBuilder()
                for (i in md5Bytes.indices) {
                    md5.append(Integer.toHexString(md5Bytes[i] and 0xFF or 0x100).substring(1, 3))
                }
                md5Key = md5.toString()
            } catch (e: NoSuchAlgorithmException) {
                e.printStackTrace()
            }
            return md5Key
        }
    }

}