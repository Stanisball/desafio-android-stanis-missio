package com.example.desafioandroid

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

open class OpahChallengeApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@OpahChallengeApplication)
            modules(listOf(networkModule, repositoryModule, viewModelModule))
        }
    }

}