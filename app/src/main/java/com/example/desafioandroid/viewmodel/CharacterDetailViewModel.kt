package com.example.desafioandroid.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.desafioandroid.model.Character
import com.example.desafioandroid.CharacterRepository
import kotlinx.coroutines.launch
import retrofit2.HttpException

class CharacterDetailViewModel(private val repository: CharacterRepository) : ViewModel() {

    private val _loading = MutableLiveData<Boolean>()

    val loading: LiveData<Boolean>
        get() = _loading

    private val _characterDetail = MutableLiveData<MutableList<Character>>()

    val characterDetail: LiveData<MutableList<Character>>
        get() = _characterDetail

    private val _error = MutableLiveData<HttpException>()

    val error: LiveData<HttpException>
        get() = _error

    fun getCharacterDetail(characterId: String) {
        viewModelScope.launch {
            try {
                _characterDetail.value = repository.getCharacterDetail(characterId).data.result
            } catch (error: HttpException) {
                _error.value = error
            }
        }

    }

}
