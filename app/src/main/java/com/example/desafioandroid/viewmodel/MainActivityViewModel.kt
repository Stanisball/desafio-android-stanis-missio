package com.example.desafioandroid.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.desafioandroid.model.Character
import com.example.desafioandroid.CharacterRepository
import kotlinx.coroutines.launch
import retrofit2.HttpException

class MainActivityViewModel(private val characterRepository: CharacterRepository) : ViewModel() {

    private val _loading = MutableLiveData<Boolean>()

    val loading: LiveData<Boolean>
        get() = _loading

    private val _characterList = MutableLiveData<MutableList<Character>>()

    val characterList: LiveData<MutableList<Character>>
        get() = _characterList

    private val _error = MutableLiveData<HttpException>()

    val error: LiveData<HttpException>
        get() = _error

    fun getCharacterList() {
        viewModelScope.launch {
            try {
                _characterList.value = characterRepository.getCharacterList().data.result
            } catch (error: HttpException) {
                _error.value = error
            }
        }

    }

}