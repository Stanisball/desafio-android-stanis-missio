package com.example.desafioandroid.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.desafioandroid.CharacterRepository
import com.example.desafioandroid.model.HQ
import kotlinx.coroutines.launch
import retrofit2.HttpException

class ExpenseCharacterHQViewModel(private val repository: CharacterRepository): ViewModel() {

    private val _loading = MutableLiveData<Boolean>()

    val loading: LiveData<Boolean>
        get() = _loading

    private val _characterExpenseHQ = MutableLiveData<MutableList<HQ>>()

    val characterExpenseHQ: LiveData<MutableList<HQ>>
        get() = _characterExpenseHQ

    private val _error = MutableLiveData<HttpException>()

    val error: LiveData<HttpException>
        get() = _error

    fun getCharacterExpenseHQ(characterId: String) {
        viewModelScope.launch {
            try {
                _characterExpenseHQ.value = repository.getCharacterExpenseHQ(characterId).data.result
            } catch (error: HttpException) {
                _error.value = error
            }
        }

    }

}
