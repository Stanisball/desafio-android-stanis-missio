package com.example.desafioandroid.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.desafioandroid.R
import com.example.desafioandroid.databinding.ActivityCharacterDetailBinding
import com.example.desafioandroid.viewmodel.CharacterDetailViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class CharacterDetailActivity : AppCompatActivity() {

    private val viewModel: CharacterDetailViewModel by viewModel()
    private lateinit var binding: ActivityCharacterDetailBinding

    private var characterId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_character_detail)
        binding.lifecycleOwner = this

        characterId = intent.getStringExtra("characterId")!!

        viewModel.getCharacterDetail(characterId)

        viewModel.characterDetail.observe(this) {
            binding.character = it[0]
        }

    }

    fun onCallExpensiveHQActivity(view: View) {
        val intent = Intent(this, ExpenseCharacterHQActivity::class.java)
        intent.putExtra("characterId", characterId)
        startActivity(intent)
    }

}