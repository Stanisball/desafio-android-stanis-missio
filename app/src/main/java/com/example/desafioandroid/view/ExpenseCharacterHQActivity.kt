package com.example.desafioandroid.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.desafioandroid.R
import com.example.desafioandroid.databinding.ActivityExpenseCharacterHqBinding
import com.example.desafioandroid.viewmodel.ExpenseCharacterHQViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class ExpenseCharacterHQActivity : AppCompatActivity() {

    private lateinit var binding: ActivityExpenseCharacterHqBinding
    private val viewModel: ExpenseCharacterHQViewModel by viewModel()

    private var characterId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_expense_character_hq)
        binding.lifecycleOwner = this

        characterId = intent.getStringExtra("characterId")!!

        viewModel.getCharacterExpenseHQ(characterId)

        viewModel.characterExpenseHQ.observe(this) {
            binding.hq = it[0]
        }

    }
}