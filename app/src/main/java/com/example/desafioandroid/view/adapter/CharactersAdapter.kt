package com.example.desafioandroid.view.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.desafioandroid.R
import com.example.desafioandroid.model.Character
import com.example.desafioandroid.view.CharacterDetailActivity

class CharactersAdapter(private var characterList: MutableList<Character>, private val context: Context) :
    RecyclerView.Adapter<CharacterViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.character_list_view_holder, parent, false)

        return CharacterViewHolder(view)
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        holder.characterName.text = characterList[position].name
        Glide
            .with(holder.itemView.context)
            .load("${characterList[position].thumbnail.path}.${characterList[position].thumbnail.extension}")
            .into(holder.characterImage)
        holder.container.setOnClickListener { onCallCharacterDetailActivity(position) }
    }

    override fun getItemCount(): Int = characterList.size

    fun update(characterList: MutableList<Character>) {
        this.characterList = characterList
        notifyDataSetChanged()
    }

    private fun onCallCharacterDetailActivity(position: Int) {
        val intent = Intent(context, CharacterDetailActivity::class.java)
        intent.putExtra("characterId", characterList[position].id)
        context.startActivity(intent)
    }
}