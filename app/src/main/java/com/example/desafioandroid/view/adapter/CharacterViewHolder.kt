package com.example.desafioandroid.view.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.desafioandroid.R

class CharacterViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val characterName: TextView = view.findViewById(R.id.character_name)
    val characterImage: ImageView = view.findViewById(R.id.character_image)
    val container: ConstraintLayout = view.findViewById(R.id.container)

}