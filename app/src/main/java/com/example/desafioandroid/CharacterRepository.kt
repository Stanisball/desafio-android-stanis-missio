package com.example.desafioandroid

import com.example.desafioandroid.utils.APIUtils

class CharacterRepository(private val api: API) {

    suspend fun getCharacterList() =
        api.getCharacterList(APIUtils.timeStamp, APIUtils.publicKey, APIUtils.getMd5Key())

    suspend fun getCharacterDetail(characterId: String) = api.getCharacterDetail(
        characterId,
        APIUtils.timeStamp,
        APIUtils.publicKey,
        APIUtils.getMd5Key()
    )

    suspend fun getCharacterExpenseHQ(characterId: String) = api.getCharacterExpenseHQ(
        characterId,
        APIUtils.timeStamp,
        APIUtils.publicKey,
        APIUtils.getMd5Key()
    )

}
