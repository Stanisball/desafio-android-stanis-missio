package com.example.desafioandroid

import com.example.desafioandroid.model.Character
import com.example.desafioandroid.model.HQ
import com.example.desafioandroid.model.MarvelResponse
import com.example.desafioandroid.model.MarvelResult
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface API {

    @GET("/v1/public/characters")
    suspend fun getCharacterList(
        @Query("ts") timestamp: String,
        @Query("apikey") apiKey: String,
        @Query("hash") hash: String
    ): MarvelResponse<MarvelResult<Character>>

    @GET("/v1/public/characters/{characterId}")
    suspend fun getCharacterDetail(
        @Path("characterId") characterId: String,
        @Query("ts") timestamp: String,
        @Query("apikey") apiKey: String,
        @Query("hash") hash: String
    ): MarvelResponse<MarvelResult<Character>>

    @GET("/v1/public/characters/{characterId}/comics")
    suspend fun getCharacterExpenseHQ(
        @Path("characterId") characterId: String,
        @Query("ts") timestamp: String,
        @Query("apikey") apiKey: String,
        @Query("hash") hash: String
    ): MarvelResponse<MarvelResult<HQ>>

}