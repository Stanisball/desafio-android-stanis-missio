package com.example.desafioandroid.model

import com.google.gson.annotations.SerializedName

data class MarvelResponse<T>(@SerializedName("data") var data: T)