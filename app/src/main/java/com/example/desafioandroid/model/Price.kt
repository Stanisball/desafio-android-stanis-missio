package com.example.desafioandroid.model

import com.google.gson.annotations.SerializedName

data class Price(
    @SerializedName("type") var type: String = "",
    @SerializedName("price") var price: Double
)