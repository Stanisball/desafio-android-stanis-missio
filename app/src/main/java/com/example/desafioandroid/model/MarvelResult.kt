package com.example.desafioandroid.model

import com.google.gson.annotations.SerializedName

data class MarvelResult<T>(@SerializedName("results") var result: MutableList<T>)