package com.example.desafioandroid.model

data class Thumbnail(
    var path: String = "",
    var extension: String = ""
)