package com.example.desafioandroid.model

import com.google.gson.annotations.SerializedName

data class HQ(
    @SerializedName("title") var title: String = "",
    @SerializedName("thumbnail") var thumbnail: Thumbnail,
    @SerializedName("description") var description: String,
    @SerializedName("prices") var price: MutableList<Price>
)