package com.example.desafioandroid.model

data class Character(
    var id: String = "",
    var name: String = "",
    var description: String = "",
    var thumbnail: Thumbnail
)